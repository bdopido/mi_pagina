import { LitElement, html } from "lit";


class PersonaStats extends LitElement{

    static get properties(){
        return{
            robins: {type: Array}
        };
    }

    constructor(){
        super();

        this.robins = [];

    }

    updated(changedProperties) {
        console.log("updated en persona-stats");

        if(changedProperties.has("robins")) {
            console.log("Ha cambiado el valor de la propiedad robins pero en persona-stats");
            
            let robinsStats = this.gatherRobinsArrayInfo(this.robins);
            this.dispatchEvent(
                new CustomEvent(
                    "updated-robins-stats",
                    {
                        detail : {
                            robinsStats: robinsStats
                        }
                    }
                )
            )
        }
    }

    gatherRobinsArrayInfo(robins) {
        console.log("gatherRobinsArrayInfo");

        let robinsStats = {};
        robinsStats.numberOfRobins = robins.length;

        console.log(robinsStats);

        return robinsStats;
    }
}
customElements.define('persona-stats', PersonaStats);