import { LitElement, html } from "lit";

class FichaPersona extends LitElement{

    static get properties(){
        return{
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    constructor(){
        super();

        this.name = "Jason Todd";
        this.yearsInCompany = 12;
        // this.updateNumber();
    }

    updated(changedProperties){

        // mapeo(oldValue, propName){
        //     console.log("Propiedad " + propName 
        //     + " ha cambiado de valor, anterior era " + oldValue)
        // }

        changedProperties.forEach(
            (oldValue, propName) => {
                console.log("Propiedad " + propName 
                + " ha cambiado de valor, anterior era " + oldValue)
            }
        )

        if(changedProperties.has("name")){
            console.log("Propiedad name cambia valor, anterior era "+
            changedProperties.get("name") + " nuevo es " + this.name);
        }

        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedad name cambia valor, anterior era "+
            changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updateNumber();
        }

    }

     render() {
         return html `
         <div>
             <label>Nombre Completo</label>
             <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"/>
             <br />
             <label>Años en la empresa</label>
             <input type="text" value="${this.yearsInCompany}" @input="${this.updateNumber}"/>
             <br />
             <input type="text" value="${this.personInfo}" disabled/>
             <br />
         </div>
         `
     }

     updateNumber(e){
         console.log("updateNumber");
         console.log(e);
                if(e.target.value >= 7){
                    this.personInfo = "lead";
                }else if(e.target.value>= 5){
                    this.personInfo = "senior";
                }else if(e.target.value >= 3){
                    this.personInfo = "team";
                }else{
                    this.personInfo = "junior";
                }
    }

     updateName(){
         console.log("updateName");

        //  console.log(Event);
        this.name = e.target.value;
        console.log(event.target.value);

     }
}

customElements.define('ficha-persona', FichaPersona);