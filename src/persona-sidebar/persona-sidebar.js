import { LitElement, html } from "lit";


class PersonaSidebar extends LitElement{

    static get properties(){
        return{
            robinsStats: {type: Object}
        };
    }

    constructor(){
        super();

        this.robinsStats = {};
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <aside>
                <section>
                    <div>
                       Hay <span>${this.robinsStats.numberOfRobins}</span> personas
                    </div>
                    <div class="mt-5">
                        <button class="btn btn-success w-100"
                        @click="${this.newRobin}">
                            <strong style="font-size: 50px">+</strong>
                        </button>
                    </div>
                </section>
            </aside>
        `
    }

    //<span class="badge bg-pill bg-primary"></span>
    newRobin(e){
        console.log("newRobin en persona-sidebar");
        console.log("Se va a crear un nuevo amigo de los petirrojos");

        this.dispatchEvent(new CustomEvent("new-robin", {}));
    }
}
customElements.define('persona-sidebar', PersonaSidebar);