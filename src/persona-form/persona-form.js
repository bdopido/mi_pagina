import { LitElement, html } from "lit";


class PersonaForm extends LitElement{

    static get properties(){
        return{
            robin: {type: Object},
            editingRobin: { type: Boolean }
        };
    }

    constructor(){
        super();

        this.resetFormData();
        
        
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input class="form-control" type="text" 
                        placeholder="Nombre completo" @input="${this.updateName}"ç
                        .value="${this.robin.name}"
                        ?disabled="${this.editingRobin}"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea class="form-control" type="text" 
                        placeholder="Perfil" rows="5"
                        @input="${this.updateProfile}"
                        .value="${this.robin.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input class="form-control" type="text" 
                        @input="${this.updateYearsInCompany}" 
                        placeholder="Años en la empresa" 
                        .value="${this.robin.yearsInCompany}"/>
                    </div>
                    <button class="btn btn-primary"
                    @click="${this.goBack}">
                        <strong>Atrás</strong>
                    </button>
                    <button class="btn btn-success"
                    @click="${this.storeRobin}">
                        <strong>Guardar</strong>
                    </button>
                </form>
            </div>
        `
    }

    goBack(e){
        console.log("goBack");
        console.log("Se ha pulsado btn atras form persona");

        
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.resetFormData();
    }

    storeRobin(e) {
        console.log("storeRobin");
        console.log(e);
        e.preventDefault();

        console.log("La propiedad name vale: " + this.robin.name);
        console.log("La propiedad profile vale: " + this.robin.profile);
        console.log("La propiedad yearsInCompany vale: " + this.robin.yearsInCompany);

        this.robin.photo = {
            src: './img/Conner.jpg',
            alt: "Superboy"
        }

        this.dispatchEvent(
            new CustomEvent(
                "persona-form-store",
            {
                detail: {
                    robin: {
                        name: this.robin.name,
                        profile: this.robin.profile,
                        yearsInCompany: this.robin.yearsInCompany,
                        photo: this.robin.photo
                    },
                    editingRobin: this.editingRobin
                }
            }
        ))
    }

    updateName(e) {
        console.log("updateName");
        console.log("Estoy guardando en la propiedad name "+ e.target.value);

        this.robin.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Estoy guardando en la propiedad profile "+ e.target.value);

        this.robin.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("Estoy guardando en la propiedad yearsInCompany "+ e.target.value);

        this.robin.yearsInCompany = e.target.value;
    }

    resetFormData(){ 
        this.robin = {};

        this.robin.name = "";
        this.robin.profile = "";
        this.robin.yearsInCompany = "";

        this.editingRobin = false;
    }
}
customElements.define('persona-form', PersonaForm);