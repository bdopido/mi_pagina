import { LitElement, html } from "lit";
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement{

    static get properties(){
        return{

            robins: {type: Array}
        };
    }

    constructor(){
        super();

    }

    render() {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
            <persona-sidebar class="col-2" @new-robin="${this.newRobin}"></persona-sidebar>
            <persona-main class="col-10" @people-updated="${this.peopleUpdated}"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-robins-stats="${this.robinsStatsUpdated}"></persona-stats>
        `
    }

    updated(changedProperties){
        console.log("updated en persona-app");

        if(changedProperties.has("robins")){
            console.log("Ha cambiado el valor de la propiedad robins en persona-app");
            this.shadowRoot.querySelector("persona-stats").robins = this.robins;
        }

        
        console.log(this.numberOfRobins);
    }

    newRobin(e) {
        console.log("newRobin en persona-app");

        this.shadowRoot.querySelector("persona-main").showRobinsForm = true;
    }

    robinsStatsUpdated(e) {
        console.log("robinsStatsUpdated en persona-app");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-sidebar").robinsStats = e.detail.robinsStats;
    }

    peopleUpdated(e) {
        console.log("peopleUpdated en persona-app");
        console.log(e.detail);


        this.robins = e.detail.robins;

    }

}
customElements.define('persona-app', PersonaApp);