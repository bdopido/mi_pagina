import { LitElement, html } from "lit";


class PersonaMainDM extends LitElement{

    static get properties(){
        return{
            robins: {type: Array},
        };
    }

    constructor(){
        super();

        this.robins = [
            {
                name: "Richard 'Dick' Grayson",
                yearsInCompany: 15,
                photo: {
                    src: "./img/Nightwing.jpg",
                    alt: "NightWing"
                },
                profile: "El primer Robin"
            } , {
                name: "Jason Peter Todd",
                yearsInCompany: 5,
                photo: {
                    src: "./img/Red_hood.png",
                    alt: "Capucha Roja"
                },
                profile: "El forajido"
            }, {
                name: "Timothy Drake",
                yearsInCompany: 8,
                photo: {
                    src: "./img/Red_robin.png",
                    alt: "Red Robin"
                },
                profile: "El mejor Robin"
            }, {
                name: "Damian Wayne",
                yearsInCompany: 2,
                photo: {
                    src: "./img/Robin.jpg",
                    alt: "Robin"
                },
                profile: "El hijo del diablo"
            },
            {
                name: "Koriand'r Anders",
                yearsInCompany: 0,
                photo: {
                    src: "./img/Starfire.jpg",
                    alt: "Starfire"
                },
                profile: "Princesa de Tamariand, miembro de los Teen Titans y de los Forajidos"
            },{
                name: "Roy Harper",
                yearsInCompany: 0,
                photo: {
                    src: "./img/Roy.jpg",
                    alt: "Arsenal"
                },
                profile: "Primer ayudante e hijo de Oliver Queen, Green Arrow"
            },{
                name: "Bartholomew Henry Allen II",
                yearsInCompany: 0,
                photo: {
                    src: "./img/impulso.jpg",
                    alt: "Impulso"
                },
                profile: "Nieto de Barry Allen, Flash"
            },{
                name: "Jonathan Samuel Kent",
                yearsInCompany: 0,
                photo: {
                    src: "./img/Jon.jpg",
                    alt: "Superboy"
                },
                profile: "Hijo de Superman y Lois Lane"
            }
        ]

    }

    updated(changedProperties){
        console.log("updated en persona-main-dm");

        if(changedProperties.has("robins")){
            console.log("Ha cambiado el valor de la propiedad robins en persona-main-dm");

            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail: {
                            robins: this.robins
                        }
                    }
                )
            )
        }
    }

}
customElements.define('persona-main-dm', PersonaMainDM);