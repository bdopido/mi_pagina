import { LitElement, html } from "lit";
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js'


class PersonaMain extends LitElement{

    static get properties(){
        return{
            robins: {type: Array},
            showRobinsForm: {type: Boolean}
        };
    }

    constructor(){
        super();
        this.showRobinsForm = false;
        this.robins = [];

    }

    render() {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <h2 class="text-center">The Robins and their friends</h2>
        <div class="row" id="robinList">
            <main class="row row-cols-1 row-cols-sm-4">
                ${this.robins.map(
                        person => html`<persona-ficha-listado 
                                        fname="${person.name}"
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"
                                        ></persona-ficha-listado>`
                )}
            </main>
        </div>
        <div class="row">
            <persona-form id="personForm" class="border rounded border-primary d-none"
            @persona-form-close="${this.robinFormClose}"
            @persona-form-store="${this.robinFormStore}"></persona-form>
        </div>
        <persona-main-dm @people-updated="${this.updatePeople}"></persona-main-dm>
        `
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        this.robins = this.robins.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se va a editar la persona de nombre " + e.detail.name);

        let chosenRobin = this.robins.filter(
            person => person.name === e.detail.name
        );

        let robin = {};
        robin.name = chosenRobin[0].name;
        robin.profile = chosenRobin[0].profile;
        robin.yearsInCompany = chosenRobin[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").robin = robin;
        this.shadowRoot.getElementById("personForm").editingRobin = true;
        this.showRobinsForm = true;
    }

    updated(changedProperties){
        console.log("updated en persona-main");
        
        if(changedProperties.has("showRobinsForm")){
            console.log("Ha cambiado el valor de showRobinsForm en persona-main");

            if(this.showRobinsForm === true){
                this.showRobinsFormData();
                
            }else{
                this.showRobinsList();
            }
        }

        if(changedProperties.has("robins")){
            console.log("Ha cambiado el valor de la propiedad robins en persona-main");

            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail: {
                            robins: this.robins
                        }
                    }
                )
            )
        }
    }
    
    showRobinsList(){
        console.log("showRobinList");
        console.log("Mostrando el listado de robins");

        this.shadowRoot.getElementById("robinList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showRobinsFormData() {
        console.log("showRobinsFormData");
        console.log("Mostrando el formulario de añadir robins");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("robinList").classList.add("d-none");
    }

    robinFormClose(e){
        console.log("robinFormClose");

        this.showRobinsForm = false;
    }

    robinFormStore(e) {
        console.log("robinFormStore");
        console.log(e.detail.robin);

        if(e.detail.editingRobin === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.name);

            // let indexRobin = this.robins.findIndex(
            //     robin => robin.name === e.detail.robin.name
            // );

            // if(indexRobin >= 0 ){
            //     console.log("Robin encontrado");

            //     this.robins[indexRobin] = e.detail.robin;
            // }

            this.robins = this.robins.map(
                robin => robin.name === e.detail.robin.name
                    ? robin = e.detail.robin : robin);
        }else{
            console.log("Se va a almacenar una persona nueva");
            // this.robins.push(e.detail.robin);
            this.robins = [...this.robins, e.detail.robin];
            // this.robins = [this.robins, e.detail.robin];
            // JS Spread syntax


        }

        // this.robins.push(e.detail.robin);
        console.log("Proceso terminado");

        this.showRobinsForm = false;
    }

    updatePeople(e) {
        console.log("updatePeople en persona main");
        this.robins = e.detail.robins;
    }
}
customElements.define('persona-main', PersonaMain);