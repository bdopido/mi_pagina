import { LitElement, html } from "lit";


class TestApi extends LitElement{

    static get properties(){
        return{
            movies: { type: Array }
        };
    }

    constructor(){
        super();
        
        this.movies = [];
        this.getMovieData();
    }

    render() {
        return html `
            ${this.movies.map(
                movie => html `
                <div>
                    La pelicula ${movie.title} fue dirigida por ${movie.director}
                </div>
                
                `
            )}
        `
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las peliculas");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Peticion completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.movies = APIResponse.results;
                console.log(APIResponse.results);
            }else if(xhr.status === 404) {
                console.log("error 404, no hay datos o no se ha podido acceder");
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send();
        console.log("Fin de getMovieData");
    }
}
customElements.define('test-api', TestApi);